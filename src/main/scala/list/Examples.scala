package list

/**
  * Created by mark on 18/03/2017.
  */
object Examples {
  def sumRecur(list:List[Double]):Double= ???


  def maxRecur(list:List[Int]):Int={
    if (list.isEmpty) Int.MinValue
    else math.max(list.head,maxRecur(list.tail))
  }
  def isExistRecur(list:List[String],n:String):Boolean={
    if (list.isEmpty) false
    else if (list.head==n) true
    else isExistRecur(list.tail,n)
  }
  def reverseRecur(list:List[Double]):List[Double]={
    if (list.isEmpty) Nil
    else reverseRecur(list.tail) :+ list.head
  }
  def combineRecur(self:List[String], that:List[String]):List[String]={
    if (that.isEmpty) self
    else combineRecur(self :+ that.head,that.tail)
  }






}
