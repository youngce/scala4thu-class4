package lambda

/**
  * Created by mark on 19/03/2017.
  */
object Examples {

  def strLengths(strs:List[String]): List[Int] =
    strs.map(str=>str.length)
  def squares(list:List[Double]): List[Double] ={
    list.map(num=>{
      math.pow(num,2)
    })
  }

  def odds(nums:List[Double])=
    nums.filter(num=>num%2==1)
  def firstLetterIsM(names:List[String])={
    names.filter(name=>name.startsWith("M"))

  }
  def squareOfOdds(nums:List[Int])={
    nums.filter(num=>num%2==1)
      .map(num=>math.pow(num,2))
    
  }







}
